package com.demo.bank.app.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.bank.app.entity.Customer;


public interface CustomerRepository extends JpaRepository <Customer, Integer> {

	Optional findByEmail(String email);

	Optional findByPassword(String password);

	
}
