package com.demo.bank.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.bank.app.entity.TransactionsAmount;



public interface Transactions1Repository extends JpaRepository<TransactionsAmount, Integer>{

}
