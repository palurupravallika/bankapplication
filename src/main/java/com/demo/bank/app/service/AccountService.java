package com.demo.bank.app.service;

import com.demo.bank.app.dto.AccountDto;
import com.demo.bank.app.dto.ApiResponse;

public interface AccountService {

	ApiResponse updateProfile(int accountid, AccountDto accountdto);

}
