package com.demo.bank.app.dto;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FundsTranferDto {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
private int FundsTranfer;
	private int benificiaryId;
	private int CustomerId;
}
