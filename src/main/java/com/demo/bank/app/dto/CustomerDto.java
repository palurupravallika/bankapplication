package com.demo.bank.app.dto;


import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CustomerDto {
	
	
	@NotBlank(message = "name is required ")
	private String name;
	
	@Min(value = 18, message = "age must be at least 18")
	private int age;

	@NotBlank(message = "email is required ")
	@Email(message = "Email should be format")
	private String email;

	@NotNull(message = "adhar is required field")
	private Long adharNo;
	
	//@Pattern(regexp="[0-9]{10}",message="Mobile number must be 10 digits")
	private Long MobileNumber;

}
