package com.demo.bank.app.exception;

import org.springframework.http.HttpStatus;


import lombok.Builder;
import lombok.Data;
@Data
@Builder
public class ErrorResponse1 {
	private HttpStatus code;
	private String message;
}
