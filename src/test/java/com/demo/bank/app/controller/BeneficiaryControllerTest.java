package com.demo.bank.app.controller;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import com.demo.bank.app.dto.ApiResponse;
import com.demo.bank.app.dto.BeneficiaryDto;
import com.demo.bank.app.service.BeneficiaryService;
@ExtendWith(SpringExtension.class)
class BeneficiaryControllerTest {
	@Mock
	private BeneficiaryService beneficiaryService;
	@InjectMocks
	private BeneficiaryController beneficiaryController;
	


	@Test
	void testAdddbeneficiary() {
		BeneficiaryDto beneficiaryDto = BeneficiaryDto.builder().beneficiaryAccountId(1).beneficiaryAccountNum("jkht765")
				.beneficiaryIFSC("bg65ft").build();
		ApiResponse apiResponse = ApiResponse.builder().httpStatus(200l).build();
		when(beneficiaryService.addbeneficiary(1, beneficiaryDto)).thenReturn(apiResponse);
	}

}
